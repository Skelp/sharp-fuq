﻿using System;
using System.Runtime.Serialization;

namespace SharpFuq
{
    [Serializable]
    internal class MissingLoopTerminationException : Exception
    {
        public MissingLoopTerminationException()
        {
        }

        public MissingLoopTerminationException(string message) : base(message)
        {
        }

        public MissingLoopTerminationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public MissingLoopTerminationException(int programCounter) 
            : base($"Expected loop termination due to loop entry in {programCounter} but found none after program code execution")
        {
        }

        protected MissingLoopTerminationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
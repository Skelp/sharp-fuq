﻿using System;
using System.Runtime.Serialization;

namespace SharpFuq
{
    [Serializable]
    internal class MemoryPointerOverflowException : Exception
    {
        public MemoryPointerOverflowException()
        {
        }

        public MemoryPointerOverflowException(string message) : base(message)
        {
        }

        public MemoryPointerOverflowException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MemoryPointerOverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
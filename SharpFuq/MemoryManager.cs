﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpFuq
{
    public class MemoryManager
    {
        const int MinimumSize = 30000;

        public byte[] Memory { get; private set; }

        private int MemoryPointer { get; set; }

        public MemoryManager(int memorySize)
        {
            if (memorySize < MinimumSize)
            {
                throw new ArgumentOutOfRangeException(nameof(memorySize), memorySize, $"Argument must not be below {MinimumSize}");
            }

            Memory = new byte[memorySize];
        }

        public void ProgressPointer()
        {
            if (MemoryPointer + 1 > Memory.Length - 1)
            {
                throw new MemoryPointerOverflowException();
            }

            MemoryPointer++;
        }

        public void RegressPointer()
        {
            if (MemoryPointer - 1 < 0)
            {
                throw new MemoryPointerUnderflowException();
            }

            MemoryPointer--;
        }

        public byte ReadCell()
        {
            return Memory[MemoryPointer];
        }

        public void WriteCell(byte value)
        {
            Memory[MemoryPointer] = value;
        }

        public void Increment()
        {
            Memory[MemoryPointer]++;
        }

        public void Decrement()
        {
            Memory[MemoryPointer]--;
        }
    }
}
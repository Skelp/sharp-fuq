﻿using System;
using System.Runtime.Serialization;

namespace SharpFuq
{
    [Serializable]
    internal class MemoryPointerUnderflowException : Exception
    {
        public MemoryPointerUnderflowException()
        {
        }

        public MemoryPointerUnderflowException(string message) : base(message)
        {
        }

        public MemoryPointerUnderflowException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MemoryPointerUnderflowException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
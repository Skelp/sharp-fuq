﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharpFuq
{
    /// <summary>
    /// Interpreter with memory management for brainfuck code.
    /// </summary>
    public class Interpreter
    {
        public MemoryManager Memory { get; private set; }

        private byte[] Program { get; set; }

        private int ProgramCounter { get; set; }

        private Stack<int> LoopEntrances { get; set; } = new Stack<int>();

        /// <summary>
        /// Delegate to write a character to the current memory cell.
        /// </summary>
        private Func<char> ReceiveChar { get; set; }

        /// <summary>
        /// Delegate to read and return a character from the current memory cell.
        /// </summary>
        private Action<char> PrintChar { get; set; }

        public Interpreter(int memorySize, string program, Func<char> receiveChar, Action<char> printChar)
        {
            Memory = new MemoryManager(memorySize);
            Program = Encoding.UTF8.GetBytes(program);
            ReceiveChar = receiveChar;
            PrintChar = printChar;
        }

        public void Run()
        {
            while (SingleStep()) ;

            if (LoopEntrances.Count > 0)
            {
                throw new MissingLoopTerminationException(LoopEntrances.Peek());
            }
        }

        public bool SingleStep()
        {
            var currentOperator = Program[ProgramCounter];

            switch ((char)currentOperator)
            {
                case '-':
                    Memory.Decrement();
                    break;

                case '+':
                    Memory.Increment();
                    break;

                case '<':
                    Memory.RegressPointer();
                    break;

                case '>':
                    Memory.ProgressPointer();
                    break;

                case '[':
                    LoopEntrances.Push(ProgramCounter);
                    break;

                case ']':
                    ProcessLoopTerminator();
                    break;

                case ',':
                    byte read = (byte)ReceiveChar();
                    Memory.WriteCell(read);
                    break;

                case '.':
                    char write = (char)Memory.ReadCell();
                    PrintChar(write);
                    break;

                default:
                    break;
            }

            ProgramCounter++;

            if (ProgramCounter == Program.Length - 1)
            {
                return false;
            }

            return true;
        }

        private void ProcessLoopTerminator()
        {
            if (Memory.ReadCell() == 0)
            {
                LoopEntrances.Pop();
            }
            else
            {
                ProgramCounter = LoopEntrances.Peek();
            }
        }
    }
}
﻿using SharpFuq;
using System;
using System.Diagnostics;
using System.IO;

namespace DemoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var helloWorld = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";

            Func<char> readFunc = () => Console.ReadKey(true).KeyChar;
            Action<char> writeAction = (c) => Console.Write(c);

            var processor = new Interpreter(30000, helloWorld, readFunc, writeAction);

            Console.WriteLine($"Executing code:\n");

            Console.ForegroundColor = ConsoleColor.White;
            Stopwatch stopwatch = Stopwatch.StartNew();
            processor.Run();
            stopwatch.Stop();
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine($"\n\nExecution took {stopwatch.ElapsedMilliseconds}ms.");
            Console.ReadLine();
        }
    }
}
# SharpFuq

SharpFuq is a [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck) interpreter with 8-bit cells written in C#.
Implemented in a class library, this interpreter was coded to be implemented on as many platforms as possible. This is done by outsourcing the way reads (Operator `,`) and writes (Operator `.`) are handled. 

## How to use
---
### Initialization

The simplest way to use this library is similar to how it is done in the demo project in this repository.
First, instanciate a new `Interpreter` object. It takes the amount of memory in bytes (minimum of 30.000), the Brainfuck code to run as a `string`, as well as a `Func<char>` and `Action<char>` for processing character inputs and outputs respectively.

The following is an example using a .NET Core Console application.

```csharp
string helloWorld = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";

Func<char> readFunc = () => Console.ReadKey(true).KeyChar;
Action<char> writeAction = (c) => Console.Write(c);

var processor = new Interpreter(30000, helloWorld, readFunc, writeAction);
```

### Execution

Execution is as simple as calling `Run();`, which will execute the entire program until either an error has been encountered or the end has been reached.
If you want to slow down execution, you can step through the program one operation at a time by calling `SingleStep();` instead. The method will return `false` if the program has executed its last operation and `true` otherwise.
